$(function(){
    
    // Calculate maximum latitude value on mercator projection
    var maxLat = Math.atan(Math.sinh(Math.PI)) * 180 / Math.PI;

    var infowindow = new google.maps.InfoWindow({});

    function initialize() {
        var lat = document.getElementById('inputLatitude').value;
        var lng = document.getElementById('inputLongitude').value;
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 3,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-container"), mapOptions);
        center_map();

        function center_map() {            
            // Get lat and lng values from input fields
            var lat = document.getElementById('inputLatitude').value;
            var lng = document.getElementById('inputLongitude').value;

            // Validate user input as numbers
            lat = (!isNumber(lat) ? 0 : lat);
            lng = (!isNumber(lng) ? 0 : lng);

            // Validate user input as valid lat/lng values
            lat = latRange(lat);
            lng = lngRange(lng);

            // Replace input values
            document.getElementById('inputLatitude').value = lat;
            document.getElementById('inputLongitude').value = lng;

            // Create LatLng object
            var mapCenter = new google.maps.LatLng(lat, lng);
            

            new google.maps.Marker({

                position: mapCenter,
                title: 'Marker title',
                map: map
            });

            // Center map
            map.setCenter(mapCenter);
        }
        // DOM event listener for the center map form submit
        $(".center_map").click(function(){
            center_map();            
        });

        google.maps.event.addListener(map, 'mouseup', function(event)  {
            // var center = map.getProjection().fromPointToLatLng(new google.maps.Point(event.pixel.x, event.pixel.y))
            document.getElementById('inputLatitude').value = event.latLng.lat();
            document.getElementById('inputLongitude').value = event.latLng.lng();
        });

        $(".current_location").click(function(){
            getCurrentLocation();           
        });
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }    
    function latRange(n) {
        //return parseFloat(n);
         return Math.min(Math.max(parseFloat(n), -maxLat), maxLat);
    }    
    function lngRange(n) {
        //return parseFloat(n);
         return Math.min(Math.max(parseFloat(n), -180), 180);
    }

    initialize();
});

function getCurrentLocation()
{
    $.ajax({
        url: "http://ip-api.com/json",
        type: 'GET',
        success: function(json)
        {
          console.log("My country is: " + json.country);
          document.getElementById('inputLatitude').value = json.lat;
          document.getElementById('inputLongitude').value = json.lon;
        },
        error: function(err)
        {
          alert("Request failed, error= " + err);
        }
      });

    // if ("geolocation" in navigator) {

    //     function geo_success(position) {
    //         document.getElementById('inputLatitude').value = position.coords.latitude;
    //         document.getElementById('inputLongitude').value = position.coords.longitude;
    //     }          
    //     function geo_error(err) {
    //         if(err.code == 1) {
    //           alert("Error: Access is denied!");
    //         } else if( err.code == 2) {
    //           alert("Error: Position is unavailable!");
    //         } else {
    //           alert("Sorry, no position available.");
    //         }
    //     }
        
    //     var geo_options = {
    //     enableHighAccuracy: true, 
    //     maximumAge        : 0, 
    //     timeout           : 5000
    //     };
    //     //var wpid = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
    //     navigator.geolocation.getCurrentPosition(geo_success, geo_error, geo_options);
    // } else {
    //     alert("Geolocation is not supported by this browser.");
    // }
}

$(document).ready(function(){
    
    $("#location_type").on("change", function(evt){
        if( this.value === "location_manual"){
            // $("#div_btn_panel").css("display", "none");
            $("#div_map").css("display", "block");
            $("#btn_cur_location").css("display", "none");
            $("#btn_center_map").css("display", "block");
        } else {
            // $("#div_btn_panel").css("display", "block");
            $("#div_map").css("display", "none");
            $("#btn_cur_location").css("display", "block");
            $("#btn_center_map").css("display", "none");      

            getCurrentLocation();           
        }
    });


    

    // $("#btn_continue").click(function(){        
    //     if(document.getElementById('inputLatitude').value == 0 || document.getElementById('inputLongitude').value == 0)
    //     {
    //         alert("Please set your location");
    //     }
    //     else if(document.getElementById('service_type').value == "") {
    //         alert("Please select service");
    //     }
    // });

});
