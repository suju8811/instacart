-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2020 at 08:41 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easyhour_db_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(50) NOT NULL,
  `password` varchar(155) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `phonenumber`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rabbit', '', 'admin', '$2y$10$6cO.VDtMRP5P7bspe0IVzOMgexq7Br4j9I0472wBD/cVhWja9LNHu', NULL, '2019-11-16 08:49:12', '2019-11-30 13:59:57'),
(3, 'Cesar', NULL, '7868179221', '$2y$10$.N5nsQvDbbOqyyaCzZ1/LO4P93a0baIIEi2gJNH8M3wlTb3tFXRXm', NULL, '2019-11-29 17:35:29', '2019-12-17 20:14:54');

-- --------------------------------------------------------

--
-- Table structure for table `chatting`
--

CREATE TABLE `chatting` (
  `id` int(10) UNSIGNED NOT NULL,
  `phonenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `writer` enum('client','admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_state` enum('new','old') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(11) NOT NULL,
  `contact_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `contact_email`) VALUES
(1, 'vpcflex@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sms_to_newuser` text NOT NULL,
  `sms_to_allusers` text NOT NULL,
  `email_to_allusers` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sms_to_newuser`, `sms_to_allusers`, `email_to_allusers`, `created_at`, `updated_at`) VALUES
(1, 'Welcome New User', 'EasyHOURS: Estimado usuario, el sistema ha sido restablecido. Puede volver a dar uso del mismo.', 'Hi, All Users Ok aaabcd', NULL, '2020-01-07 16:28:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_05_03_000001_create_customer_columns', 2),
(5, '2019_05_03_000002_create_subscriptions_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `principles`
--

CREATE TABLE `principles` (
  `id` int(11) NOT NULL,
  `content` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `principles`
--

INSERT INTO `principles` (`id`, `content`, `created_at`, `updated_at`) VALUES
(15, 'Welcome to EasyHOURS, your first choice to catch OnDemand Batches!', '2019-11-29 17:48:28', '2019-11-29 17:48:28'),
(16, 'Bienvenido a EasyHOURS, tu primera opcion y mas certera, para capturar batches de OnDemand!', '2019-11-29 17:49:12', '2019-11-29 17:49:12');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_option`
--

CREATE TABLE `stripe_option` (
  `id` int(11) NOT NULL,
  `stripe_pub_key` text NOT NULL,
  `stripe_secret_key` text NOT NULL,
  `amount` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stripe_option`
--

INSERT INTO `stripe_option` (`id`, `stripe_pub_key`, `stripe_secret_key`, `amount`, `currency`, `created_at`, `updated_at`) VALUES
(1, 'pk_live_KUq6p0AyXcWGPQHcXh4i6QLk', 'sk_live_Tpwv1DrXmx2gVJpSa2grD1Aa', 3117, 'USD', NULL, '2019-12-16 19:41:54');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `started_date` timestamp NULL DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `userid`, `started_date`, `amount`, `currency`, `created_at`, `updated_at`) VALUES
(1, 16, '2019-12-13 21:24:53', 3000, 'USD', '2019-12-13 21:24:53', '2019-12-13 21:24:53'),
(2, 26, '2019-12-16 16:58:08', 3000, 'USD', '2019-12-16 16:58:08', '2019-12-16 16:58:08'),
(3, 16, '2019-12-21 11:26:36', 3117, 'USD', '2019-12-21 11:26:36', '2019-12-21 11:26:36'),
(4, 52, '2019-12-22 03:23:32', 3117, 'USD', '2019-12-22 03:23:32', '2019-12-22 03:23:32'),
(5, 51, '2019-12-22 11:07:09', 3117, 'USD', '2019-12-22 11:07:09', '2019-12-22 11:07:09'),
(6, 54, '2019-12-23 23:44:19', 3117, 'USD', '2019-12-23 23:44:19', '2019-12-23 23:44:19'),
(7, 53, '2019-12-24 13:13:50', 3117, 'USD', '2019-12-24 13:13:50', '2019-12-24 13:13:50'),
(8, 16, '2019-12-28 11:35:59', 3117, 'USD', '2019-12-28 11:35:59', '2019-12-28 11:35:59'),
(9, 57, '2019-12-28 16:34:51', 3117, 'USD', '2019-12-28 16:34:51', '2019-12-28 16:34:51'),
(10, 47, '2019-12-29 01:16:48', 3117, 'USD', '2019-12-29 01:16:49', '2019-12-29 01:16:49'),
(11, 52, '2019-12-29 07:24:33', 3117, 'USD', '2019-12-29 07:24:33', '2019-12-29 07:24:33'),
(12, 44, '2019-12-29 13:43:43', 3117, 'USD', '2019-12-29 13:43:43', '2019-12-29 13:43:43'),
(13, 25, '2019-12-30 12:43:47', 3117, 'USD', '2019-12-30 12:43:47', '2019-12-30 12:43:47'),
(14, 58, '2019-12-30 15:54:34', 3117, 'USD', '2019-12-30 15:54:34', '2019-12-30 15:54:34'),
(15, 54, '2019-12-31 15:25:23', 3117, 'USD', '2019-12-31 15:25:23', '2019-12-31 15:25:23'),
(16, 59, '2020-01-03 04:21:56', 3117, 'USD', '2020-01-03 04:21:56', '2020-01-03 04:21:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_running` int(2) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `subscription_expired` tinyint(1) NOT NULL DEFAULT 1,
  `subscription_started_date` timestamp NULL DEFAULT NULL,
  `subscription_end_date` timestamp NULL DEFAULT NULL,
  `paid_amount` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `admin_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phonenumber`, `email`, `city`, `password`, `location`, `remember_token`, `ip_address`, `extension`, `is_running`, `active`, `subscription_expired`, `subscription_started_date`, `subscription_end_date`, `paid_amount`, `created_at`, `updated_at`, `admin_id`) VALUES
(1, 'Rabbit', '7860000000', 'redhat2@gmail.com', NULL, '$2y$10$BUIpM/r.FHL6p8NDKCrIP.jlqLE0UZEiYgVf.Yw4zEEHFe103gAOm', '', '6pCt6gMlBQV9YdRNLK5VFvXIoSN2T6r1UaGqehNaaX6z8m8FumF42uJAGXQh', 'localhost', 'exe', 0, 1, 0, '2019-11-29 16:00:00', '2020-11-29 16:00:00', 0, '2019-11-06 12:30:31', '2019-11-29 17:42:16', 0),
(4, 'suju', '1554224820', 'redhat4@gmail.com', NULL, '$2y$10$1q0/tfGSsMJl94Pey8EnGOarpwP1ze/ElksyXdgvcfKLR1vMMp1iC', '', NULL, '52.206.230.16 ', NULL, 0, 1, 1, NULL, NULL, 0, '2019-11-05 17:56:07', '2019-11-10 07:52:30', 0),
(16, 'Eduardo Avero', '7868569213', 'eduardoavero1@gmail.com', NULL, '$2y$10$IWDmp5KEJIYsEOcP0guuiOclV2eZyVwnRE6vMvY/jdF04dvvCXWFe', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-02-03 07:35:59', '2020-02-10 07:35:59', 9234, '2019-11-29 17:40:34', '2020-02-06 00:00:04', 0),
(17, 'Rafael Gonzalez', '7862381614', 'miaflex1212@gmail.com', NULL, '$2y$10$93I4agWDT4EDFAhnNC2Z.Omj5kOLWW3y0J6akx/8KUIJBjeRNe2rq', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-11-29 16:00:00', '2020-12-06 16:00:00', 0, '2019-11-29 17:41:33', '2020-01-20 19:46:34', 0),
(18, 'Cesar Gonzalez', '7866295463', 'cesardavidgonzalez@gmail.com', NULL, '$2y$10$GyqVC/bv2g1r3hQ8f3xNb.aqo8o7Y149iZaBlEUdGdpDkRni5Csyu', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-11-28 23:00:00', '2020-11-28 23:00:00', 0, '2019-11-29 17:45:36', '2020-02-05 20:37:33', 0),
(19, 'Edwind Arteaga', '3057678617', 'eduind.3@gmail.com', NULL, '$2y$10$2N5MdLCM19MurDuHusIqSeW9kyoGeK.mh0NSxc6RaJ.7N9xFmi/0q', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-11-28 23:00:00', '2019-12-19 23:00:00', 0, '2019-12-01 16:16:09', '2019-12-10 14:45:48', 0),
(20, 'Acedo Rodriguez', '7866005683', 'acedo2327@gmail.com', NULL, '$2y$10$eK.RMmQrVP.EVMoIVX/Ml.B9cTAbTSZFW6HJIAB3.HoxPEhRboBpi', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-06 23:00:00', '2019-12-13 23:00:00', 0, '2019-12-02 14:11:00', '2019-12-17 18:48:38', 0),
(22, 'Jorge', '7866302517', NULL, NULL, '$2y$10$6H2RpcZa6qcrCghvuJKG6OXtWF7FTDOXcehfDJYbaAkVkyzI5MqYy', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-06 15:59:40', '2019-12-06 15:59:40', 0),
(23, 'Mairim Cocco', '7863983799', 'mairimcocco@hotmail.com', NULL, '$2y$10$PoIziZW6XSzZ4y8rxXM7tu9.Hr.vsv2I7n/1T8nC2trytb2QZpknK', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-07 23:00:00', '2020-12-07 23:00:00', 0, '2019-12-06 17:52:16', '2020-02-05 16:19:10', 0),
(24, 'Elvis Diaz', '7866359214', 'elvisdiaz94@gmail.com', NULL, '$2y$10$HGHvOZPhbPGY6DfGGMVnFez.b07qhrWBaM/2ipPKiIuDhJogzkX1O', NULL, NULL, '13.92.247.125', 'exe', 0, 0, 1, '2019-12-05 23:00:00', '2019-12-12 23:00:00', 0, '2019-12-06 20:51:26', '2019-12-16 22:32:06', 0),
(25, 'Johny j surhtt', '7863428346', 'surhttj@gmail.com', NULL, '$2y$10$tF258xPJjFNqVX9noX.EpeQpNhAke6imMoi/oGDzPC7i4/Con6aJS', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-01-06 12:43:47', '2020-02-05 12:43:47', 3117, '2019-12-07 12:57:21', '2020-02-05 12:59:38', 0),
(26, 'Ricardo Albarracin', '9549910185', 'ricardoalbac2021@gmail.com', NULL, '$2y$10$HtrhHCfIB74hBBlCjJt6yu/RNBx/dpfAY0EN80N2XSsbS6T2gfL0u', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-16 16:58:08', '2019-12-23 16:58:08', 3000, '2019-12-08 14:51:59', '2019-12-23 13:22:48', 0),
(27, 'Luis Hernandez', '4074163575', 'peaxcompany@gmail.com', NULL, '$2y$10$WSKurwXd0s9qyY.2THF2UudbZTh3bAeS5epK3kyyTGvhXwQCtv92C', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-08 21:40:35', '2019-12-17 01:26:35', 0),
(28, 'Jose Perdomo', '4077220353', 'perdomoflex18@gmail.com', NULL, '$2y$10$.RW8WPz9N8zrMumjJnswse7d.1gLzaNLao8mDj322u3UGhc0Uvyxy', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-08 23:00:00', '2019-12-15 23:00:00', 0, '2019-12-08 23:00:23', '2019-12-17 22:14:04', 0),
(29, 'Jose', '7864318577', NULL, NULL, '$2y$10$qE1cUbq19HKeZp8m6PUZguzFbOlg4Xbemdoq8nvTVkmkjeeGuhf02', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-08 23:25:01', '2019-12-08 23:25:01', 0),
(31, 'Kever Mendoza', '7866121760', 'ksajidh@yahoo.com', NULL, '$2y$10$..mO36.0SnfRVQE/0gFKHOps1Uetl03hk8BmQN2JSSwCOvW41Yc4e', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-08 23:00:00', '2019-12-15 23:00:00', 0, '2019-12-09 02:17:30', '2019-12-16 00:39:02', 0),
(33, 'Flor Canton', '3214427104', 'Florurdaneta5@gmail.com', NULL, '$2y$10$KTNdqjOxyAj.l9.ybHsXtOpnm.75R/dYV8QHSSTWUBC5eyPJ498gC', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-10 23:00:00', '2019-12-17 23:00:00', 0, '2019-12-09 17:35:59', '2019-12-15 16:40:23', 0),
(35, 'Jos? ?ngel Gonz?lez', '7868231054', NULL, NULL, '$2y$10$4RibwhHbblHdDrX/myjt..yjwAc/bli8Ob2FZQ7q5/r75Le/hTLve', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-08 23:00:00', '2019-12-15 23:00:00', 0, '2019-12-09 20:43:38', '2019-12-16 19:35:07', 0),
(36, 'Saul', '3056099719', NULL, NULL, '$2y$10$TCn7nZ.AfB81t1/JR2VWkO8/uTm8MdCJiwlSyEd0SKDxHBeIMzpvi', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-10 17:57:13', '2019-12-10 17:57:13', 0),
(37, 'Carolina Davila', '7867314268', NULL, NULL, '$2y$10$WK0.yZQwjgIgEj07Mz4PkuAgjhwi61tn/iIFAyIye5w983Crv.s7e', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-10 17:58:25', '2019-12-10 17:58:25', 0),
(38, 'Daniel robles', '7866470154', 'robles281287@gmail.com', NULL, '$2y$10$daBa8PTSx07Q/M/3oarr7u7JQETF6KsnoO7MPCerFYKWmgndzS9qS', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-27 23:00:00', '2020-01-03 23:00:00', 0, '2019-12-10 18:07:45', '2020-01-13 18:05:49', 0),
(39, 'Paola', '7865870092', NULL, NULL, '$2y$10$pUjv.pThM8cXFmXVldqHKuvBlZ6M2s.Ct74/3BOWPMjNmG.e/Ytgu', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-10 18:20:29', '2020-01-12 14:36:01', 0),
(40, 'Juan Quintero', '7864207847', 'coolfire52@gmail.com', NULL, '$2y$10$ifEV4Us8K0rIQi3TUYiFtuec0viUOowGT0v//eFd8cU.43SaJ.3H6', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-10 23:00:00', '2019-12-17 23:00:00', 0, '2019-12-10 22:19:50', '2019-12-20 23:43:43', 0),
(41, 'Dany Melo', '7864366549', NULL, NULL, '$2y$10$Yr5O3HS/yoYAJWJRx3Isw.aACYzeHRtTmORqhexQo7Hib5EMwdMrS', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-11 12:27:51', '2019-12-11 12:27:51', 0),
(42, 'Richard', '7862055459', NULL, NULL, '$2y$10$.gLn8Mek1ycpY9NWYDXAzu7YEARyC04xuErzoXnA1Hvwt.M/Z4gpi', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, 0, '2019-12-11 15:49:30', '2019-12-11 15:49:30', 0),
(43, 'Diane', '5123175023', 'chaparra4530@yahoo.com', NULL, '$2y$10$Lazyqu4ufKYclhMyKMaXz.jd6ct4jc5c06okgfg7IgnofePcDV3eK', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-11 23:00:00', '2019-12-18 23:00:00', 0, '2019-12-12 02:28:36', '2019-12-16 11:07:07', 0),
(44, 'RTamarys Pupo Vazquez', '3057055314', 'bionicvaporjcap@gmail.com', NULL, '$2y$10$A2JNd1yHXdJ8F0suzeWfh..WjYlJr6MHG0EIqBLcwMqCXBsLxrzje', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-02-02 07:43:43', '2020-02-08 13:43:43', 3117, '2019-12-12 03:48:46', '2020-02-05 21:17:10', 0),
(46, 'Paola acosta', '4044530714', 'Pao_acostag@hotmail.com', NULL, '$2y$10$vm/SlJ72Ee0.aDZgfCgaFOOEfKHmvYN7dWK2SagA47O/w9xWUv2e6', NULL, NULL, '13.92.247.125', 'exe', 0, 0, 0, NULL, NULL, 0, '2019-12-12 18:49:42', '2019-12-19 17:18:45', 0),
(47, 'Javier Marquez', '7863789508', 'javier.marquez.fanta@gmail.com', NULL, '$2y$10$EaWy5WCqrGdi73IY7yIAa.WMqyXO97xAVY5JJFMb9VZnhYe95L9zm', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-01-25 09:30:48', '2020-02-01 10:30:48', 3117, '2019-12-14 00:53:30', '2020-02-05 01:54:13', 0),
(51, 'Diego Perez Alvarado', '4074574863', 'dperez387@hotmail.com', NULL, '$2y$10$bJ3f.rSESlkgLnO0ok3tle18rabFhVmgOCRZzf290ALSStZXGiyDm', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-30 10:07:09', '2020-02-06 10:07:09', 3117, '2019-12-15 14:05:46', '2020-02-05 21:58:57', 0),
(52, 'Marco rodriguez', '3059883013', 'marlmichel@hotmail.com', NULL, '$2y$10$gV7/qo6I0UmgBHezej0eOOpyWukplutJX7g.BEGDIwsgwqwHWwYwG', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-01-05 19:20:33', '2020-01-12 19:20:33', 6234, '2019-12-15 22:59:32', '2020-01-13 02:07:24', 0),
(53, 'Katiuska Navas', '7864680425', 'bernardeth1986@gmail.com', NULL, '$2y$10$J6BO3gav.xbDYRLzdiXb.eRTcNpAc5wSrCUUyIByBMpMtfdyfcTZC', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-24 13:13:50', '2019-12-31 13:13:50', 3117, '2019-12-16 18:16:06', '2020-01-03 14:33:10', 0),
(54, 'Ines Parra', '7869148866', 'inesparra@hotmail.com', NULL, '$2y$10$V3fLfP2ppDb41fqKK2GFCOtHpqBu3jDrNfTplFsjyWYUK3UDiayUG', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-02-01 06:25:23', '2020-02-07 15:25:23', 6234, '2019-12-17 17:18:54', '2020-02-05 19:31:48', 0),
(55, 'Rosa Fajardo', '7862816659', 'rosafajardo2518@gmail.com', NULL, '$2y$10$zPr1lHKUiBPt7cM6dkRdpO5JK3zySMo1.cPlHp.nCVUhCfWzLeGiO', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2019-12-13 23:00:00', '2019-12-20 23:00:00', 0, '2019-12-19 17:09:25', '2020-02-05 23:29:28', 0),
(56, 'Gisela Garcia', '3218307224', 'giselagarcia22@hotmail.com', NULL, '$2y$10$XAwp4IBVWUZJoh/2InM39ONThC5uCgsWKIg.9zhbMViDISW8MrZIm', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-26 23:00:00', '2020-01-02 23:00:00', 0, '2019-12-26 18:21:14', '2019-12-29 16:18:38', 0),
(57, 'alberto de filippis', '7863284383', 'albertopepe22@gmail.com', NULL, '$2y$10$Tf1wHZNeDWBJKE/4UdVMhefk.nCT9Jn150wp1rjWcyvabE6S/k4UW', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-19 16:34:51', '2020-02-10 16:34:51', 3117, '2019-12-27 17:57:35', '2020-02-05 20:27:52', 0),
(58, 'Janier Sanchez', '7865539114', 'janier.sanchez@gmail.com', NULL, '$2y$10$HIyFr7ResFthuNxVoHLDzuTp2RVAAmtcCTLV5J4.S.dNn/psk9VUa', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2019-12-30 15:54:34', '2020-01-06 15:54:34', 3117, '2019-12-28 16:59:15', '2020-01-03 14:53:32', 0),
(59, 'Victor Angulo', '5127136537', 'victorangulo19@gmail.com', NULL, '$2y$10$rEgnU0iuSJ.UWnOQoQZ6SOmFzxdgBejkwPGH2M2zEYNCHh29AhgTe', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-03 04:21:56', '2020-01-10 04:21:56', 3117, '2020-01-02 01:30:35', '2020-01-07 13:55:05', 0),
(60, 'Earlyne Barrios', '9546188989', 'lomi05@hotmail.com', NULL, '$2y$10$WPt2N.N6jileSzmWJhAItuCyD4wiCdQOgT/Oza0U7eH/LTHgEkejO', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-30 15:54:34', '2020-02-06 15:54:34', 0, '2020-01-02 02:43:11', '2020-02-05 19:37:19', 0),
(61, 'Manuel Campo', '7864041819', 'amzflexx@gmail.com', NULL, '$2y$10$SfHqULbFTTZlyB7PnFeIeOcgYeohPReMnb3eD1B7oZ3bIClg2Yl6W', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-22 15:54:34', '2020-01-30 15:54:34', 0, '2020-01-04 02:39:47', '2020-01-24 17:18:38', 0),
(63, 'Alexander', '7864759648', NULL, NULL, '$2y$10$YRAQ3VQzKJl5uQ3uy5usKOXeQQ8Y2pnIrPnYIrfdYR6UzFqHaDv2m', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-01-05 23:51:13', '2020-01-05 23:51:13', 0),
(64, 'Johny', '4806305720', NULL, NULL, '$2y$10$BHt8Oile4U7jGByy2v5DO.D87AVEKr4IQ3qVj0B0hqHLNdirebWqK', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-01-08 21:35:39', '2020-01-08 21:35:39', 0),
(65, 'Eduardo Petit', '3059225495', 'Eduardopetitmarino@gmail.com', NULL, '$2y$10$DpG5cVM8/KS2I4BkfP8vkO8Uy6SQ104ebICjh6XJpswMYvM370Uj.', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 1, '2020-01-21 15:54:34', '2020-01-28 15:54:34', 0, '2020-01-13 17:24:27', '2020-01-29 14:32:06', 0),
(66, 'Heneyda gomez', '7863327289', 'heneydagomez@hotmail.com', NULL, '$2y$10$B0APXX6E29zu95uPewbI.uUrLVYJ0f9GHnlatvaV6yFsPQwh.trFe', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-21 15:54:34', '2020-01-28 15:54:34', 0, '2020-01-18 22:19:56', '2020-01-24 19:29:38', 0),
(67, 'Mar?a Parra', '7868173337', NULL, NULL, '$2y$10$Dc56EpICskEeaoxOBWmSDe7iNFIEgrMvC8w01XSeftZntDyX1jkbC', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-01-20 16:24:10', '2020-01-20 16:24:10', 0),
(68, 'Yoselin', '4705483568', NULL, NULL, '$2y$10$yiAu2VdwbfiMJL3EI7AnHeCe2MIUiGNVRVKV/inSzYAf.Q19h0XQO', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-01-23 21:53:12', '2020-01-23 21:53:12', 0),
(69, 'ayman', '6146486606', NULL, NULL, '$2y$10$4VrG1JfZ1WsznX5Op/NEmOhzfLcCbPxZVcjhk.oX9SkXE1IhxcT4O', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-01-25 08:59:02', '2020-01-25 08:59:02', 0),
(70, 'Camila', '7865670596', 'amatujodidavida@gmail.com', NULL, '$2y$10$gmzlB/is6tbCFEKziihgS.YqlJRKxc4bFyH.uy08idZeb905Igw4O', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-21 15:54:34', '2020-01-28 15:54:34', 0, '2020-01-27 01:10:30', '2020-01-28 14:17:47', 0),
(71, 'Samir Sarquis', '7707785182', NULL, NULL, '$2y$10$qvECfhExiI5gF4DLZEClyuDdk2uzuStG.gvhvkkyhruuARpgiXqne', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-01-30 14:50:23', '2020-01-30 14:50:23', 0),
(72, 'Cesar Gonzalez', '7868179221', 'cesargshopper@gmail.com', NULL, '$2y$10$YBvyu3tlS0NMvkE6p1C7g.f8UKq/o.KNXO80Wh1ABNcTevnFers.G', NULL, NULL, '13.92.247.125', 'exe', 0, 1, 0, '2020-01-01 15:54:34', '2020-12-30 15:54:34', 0, '2020-01-30 14:53:40', '2020-02-05 20:37:20', 0),
(73, 'Daniel Gonzalez', '7868321375', NULL, NULL, '$2y$10$vvoRgaB2vpHastTh.z.gy.Rxftd0rxp3OnvEt9/DZsyGZ12nxdncu', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-02-05 04:06:24', '2020-02-05 04:06:24', 0),
(74, 'Daniel Gonzalez', '7868321372', NULL, NULL, '$2y$10$gWNVmJ2GfGmd4xCzcArtHun1GwhmwWVRg0CM3x8hlPQQPy1wZKG9S', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-02-05 16:30:56', '2020-02-05 16:30:56', 0),
(75, 'Enyer vargas', '3059223625', NULL, NULL, '$2y$10$M107yuIIFEYxRPqsZxn4N.8AuxJ1mWTTht1.ONolr60ZyIg6IP2qe', NULL, NULL, 'localhost', NULL, 0, 0, 1, NULL, NULL, 0, '2020-02-06 02:34:56', '2020-02-06 02:34:56', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phonenumber` (`phonenumber`);

--
-- Indexes for table `chatting`
--
ALTER TABLE `chatting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `principles`
--
ALTER TABLE `principles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stripe_option`
--
ALTER TABLE `stripe_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phonenumber_unique` (`phonenumber`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `chatting`
--
ALTER TABLE `chatting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `principles`
--
ALTER TABLE `principles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `stripe_option`
--
ALTER TABLE `stripe_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
